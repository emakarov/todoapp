from django.shortcuts import render,render_to_response
from django.template import Template,RequestContext, loader, Context

# Create your views here.

def index(request):
  return render_to_response('base.html', {}, context_instance = RequestContext(request))