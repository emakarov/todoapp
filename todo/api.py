from tastypie import fields
from tastypie.resources import ModelResource
from tastypie.authentication import MultiAuthentication,SessionAuthentication
from tastypie.constants import ALL,ALL_WITH_RELATIONS
from django.conf.urls import patterns, include, url
import datetime
from todo import models
from tastypie.authorization import DjangoAuthorization

class TaskResource(ModelResource):
  class Meta:
    queryset = models.Task.objects.all()
    resource_name = 'todo/task'
    authorization = DjangoAuthorization()
    include_resource_uri = False
    always_return_data = True