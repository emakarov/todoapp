from django.contrib import admin
from models import *

class TaskAdmin(admin.ModelAdmin):
    list_display = ['title']

admin.site.register(Task,TaskAdmin)