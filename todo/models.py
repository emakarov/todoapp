from django.db import models
from django.utils.translation import ugettext_lazy as _

# Create your models here.

class Task(models.Model):
  title = models.CharField(_("Title"), max_length=255, help_text = "title")
  text = models.TextField(_("Text"),blank=True)
  
  def __unicode__(self):
    return self.title

  class Meta:
    verbose_name = _("Task")
    verbose_name_plural = _("Tasks")