from django.conf.urls import patterns, include, url
from django.conf import settings
from tastypie.api import Api

v1_api = Api(api_name='v1')

from todo.api import *
v1_api.register(TaskResource())

urlpatterns = patterns('',
    (r'', include(v1_api.urls)),
)