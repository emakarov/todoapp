from django.conf.urls import patterns, include, url

from django.contrib import admin
admin.autodiscover()

urlpatterns = patterns('',
    # Examples:
    # url(r'^$', 'todoapp.views.home', name='home'),
    # url(r'^blog/', include('blog.urls')),

    url(r'^admin/', include(admin.site.urls)),
    (r'^api/',include('todoapp.api_urls')),
    url(r'', 'todo.views.index'),
)

urlpatterns += patterns('django.contrib.staticfiles.views',
        (r'^favicon.ico', 'serve', {'path' : 'favicon.png'}),
        url(r'^static/(?P<path>.*)$', 'serve'),
        url(r'^[a-z][a-z]/static/(?P<path>.*)$', 'serve'),
        url(r'^media/(?P<path>.*)$', 'serve'),
)